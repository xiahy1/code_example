package service;

/**
 * 聊天会话表
 *
 * @author xiahy
 * @date 2023-06-14 20:16:47
 */
public interface LazadaChatInitSessionAndMessageService {

    /**
     *  初始化会话和对应的消息
     */
    void initSessionAndMessage(Long channelManageId);

    /**
     * 将session和message 插入数据库中
     * @param dto
     * @param sessionInfo
     * @param date 刷新时间
     * @param curTime
     * @param startTime
     */
    void addSessionAndMessageToDB(ChatInitSessionAndMessageDTO dto, LazadaSessionInfoDTO sessionInfo, Date date, Long curTime, Long startTime);

    /**
     *  处理来自三方平台的 session 和 message
     */
    void handleSessionAndMessage(ChatInitSessionAndMessageDTO chatInitSessionAndMessageDTO);

}

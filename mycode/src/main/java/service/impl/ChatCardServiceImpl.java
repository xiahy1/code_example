package service.impl;

public class ChatCardServiceImpl implements ChatCardService {
    //***********************************lazada*****************************************************//
    @Autowired
    private LazadaProductQueryClient lazadaProductQueryClient;

    @Autowired
    private LazadaOrderOperationClient lazadaOrderOperationClient;

    @Autowired
    private LazadaPromotionClient lazadaPromotionClient;

    @Autowired
    private ChannelManageService channelManageService;

    @Autowired
    private LazadaReverseOrderClient lazadaReverseOrderClient;

    @Autowired
    private LazadaVideoClient lazadaVideoClient;

    //***********************************shopee api *****************************************************//
    @Autowired
    private ShopeeProductQueryClient shopeeProductQueryClient;

    @Autowired
    private ShopeeOrderClient shopeeOrderClient;

    @Autowired
    private ShopeeVoucherClient shopeeVoucherClient;

    //***********************************公共接口*****************************************************//
    /**
     * 获取商品信息
     * @param channelManageId
     * @param channelId
     * @param param
     * @return
     */
    @Override
    public ProductMessage getProductForChat(Long channelManageId, Long channelId, ChatCardProductParam param) {
        if(ChannelEnum.LAZADA.getChannelId().equals(channelId)){
            return this.getLazadaProduct(channelManageId,channelId,param);
        }else if(ChannelEnum.SHOPEE.getChannelId().equals(channelId)){
            return this.getShopeeProduct(channelManageId,channelId,param.getItemId());
        }
        return null;
    }

    /**
     * 获取订单信息
     * @param channelManageId
     * @param channelId
     * @param orderId
     * @return
     */
    @Override
    public OrderMeaasge getOrderForChat(Long channelManageId, Long channelId, String orderId) {
        if(ChannelEnum.LAZADA.getChannelId().equals(channelId)){
            return this.getLazadaOrder(channelManageId,channelId,orderId);
        }else if(ChannelEnum.SHOPEE.getChannelId().equals(channelId)){
            return this.getShopeeOrder(channelManageId,channelId,orderId);
        }
        return null;
    }

    @Override
    public CouponMessage getSellerVoucherForChat(Long channelManageId, Long channelId, String promotionId, String voucherType) {
        if(ChannelEnum.LAZADA.getChannelId().equals(channelId)){
            return this.getLazadaSellerVoucher(channelManageId, channelId, promotionId, voucherType);
        }else if(ChannelEnum.SHOPEE.getChannelId().equals(channelId)){
            return this.getShopeeSellerVoucher(channelManageId, channelId, promotionId);
        }
        return null;
    }

    @Override
    public ReturnAndRefundMessage getReturnAndRefundForChat(Long channelManageId, Long channelId, String reverseOrderId) {
        if(ChannelEnum.LAZADA.getChannelId().equals(channelId)){
            return this.getLazadaReturnAndRefund(channelManageId, channelId, reverseOrderId);
        }else if(ChannelEnum.SHOPEE.getChannelId().equals(channelId)){
            return this.getShopeeReturnAndRefund(channelManageId, channelId, reverseOrderId);
        }
        return null;
    }

    @Override
    public VideoMessage getVideoForChat(Long channelManageId, Long channelId, String videoId) {
        if(ChannelEnum.LAZADA.getChannelId().equals(channelId)){
            return this.getLazadaVideoForChat(channelManageId, channelId, videoId);
        }
        return null;
    }

    //**********************************************lazada******************************************************************************//
    /**
     * 获取lazada的视频信息
     * @param channelManageId
     * @param channelId
     * @param videoId
     * @return
     */
    private VideoMessage getLazadaVideoForChat(Long channelManageId, Long channelId, String videoId) {
        log.info("getLazadaVideoForChat channelManageId:{} channelId:{} sessionId:{} videoId:{}", channelManageId, channelId, videoId);
        VideoInfoDTO videoInfoDTO = null;
        ApiResultVO<VideoInfoDTO> re = null;
        try{
            re = lazadaVideoClient.getVideoDetail(channelManageId, Long.valueOf(videoId));
            log.info("getLazadaVideoForChat getVideoDetail re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                videoInfoDTO = re.getResult();
                return this.convertLazadavideoToVideoMessage(videoId,videoInfoDTO);
            }
        }catch (Exception e){
            log.error("getLazadaVideoForChat exception", e);
        }
        return null;
    }

    /**
     * 将lazada的视频信息转换成消息卡片所需要的信息
     * @param videoId
     * @param videoInfoDTO
     * @return
     */
    private VideoMessage convertLazadavideoToVideoMessage(String videoId, VideoInfoDTO videoInfoDTO) {
        VideoMessage videoMessage = new VideoMessage();
        videoMessage.setVideoUrl(videoInfoDTO.getVideo_url());
        videoMessage.setVideoId(videoId);
        videoMessage.setImgUrl(videoInfoDTO.getCover_url());
        videoMessage.setTxt(videoInfoDTO.getTitle());
        return videoMessage;
    }

    /**
     * 获取lazada的退款信息
     * @param channelManageId
     * @param channelId
     * @param reverseOrderId
     * @return
     */
    private ReturnAndRefundMessage getLazadaReturnAndRefund(Long channelManageId, Long channelId, String reverseOrderId) {
        log.info("getLazadaReturnAndRefund channelManageId:{} channelId:{} reverseOrderId:{}",channelManageId,channelId,reverseOrderId);
        LazadaReverseOrderDetailDTO lazadaReverseOrderDetailDTO = null;
        ApiResultVO<LazadaReverseOrderDetailDTO> re = null;
        try{
            re = lazadaReverseOrderClient.getReverseOrderDetail(channelManageId, Long.valueOf(reverseOrderId));
            log.info("getLazadaReturnAndRefund getReverseOrderDetail re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                lazadaReverseOrderDetailDTO = re.getResult();
                return this.convertLazadaRefundToReturnAndRefundMessage(channelManageId,lazadaReverseOrderDetailDTO);
            }
        }catch (Exception e){
            log.error("getLazadaReturnAndRefund exception", e);
        }
        return null;
    }

    /**
     * 将lazada的退款信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param lazadaReverseOrderDetailDTO
     * @return
     */
    private ReturnAndRefundMessage convertLazadaRefundToReturnAndRefundMessage(Long channelManageId, LazadaReverseOrderDetailDTO lazadaReverseOrderDetailDTO) {
        ReturnAndRefundMessage returnAndRefundMessage = new ReturnAndRefundMessage();
        Map<String, ReturnAndRefundProductMessage> map = new HashMap<>();
        Set<String> reasonText = new HashSet<>();
        returnAndRefundMessage.setTradeOrderId(String.valueOf(lazadaReverseOrderDetailDTO.getTrade_order_id()));
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        returnAndRefundMessage.setCurrency(SiteEnum.getSimbolByCode(channelManageDTO.getShopeeRegion().toLowerCase()));
        List<LazadaReverseOrderLineDTO> reverseOrderLineDTOList = lazadaReverseOrderDetailDTO.getReverseOrderLineDTOList();
        if(CollectionUtil.isNotEmpty(reverseOrderLineDTOList)){
            //三方返回的是时间戳
            returnAndRefundMessage.setOrderCreateTime(String.valueOf(reverseOrderLineDTOList.get(0).getTrade_order_gmt_create()));
            //卖家sku的信息
            for(LazadaReverseOrderLineDTO dto : reverseOrderLineDTOList){
                //退款订单信息
                returnAndRefundMessage.getRefundTotalPrice(dto.getRefund_amount());
                returnAndRefundMessage.getRefundTotalNum(Constant.INTEGER_ONE);
                reasonText.add(dto.getReason_text());
                //退款商品信息
                if(map.containsKey(dto.getProductDTO().getSku())){
                    ReturnAndRefundProductMessage refundProductMessage = map.get(dto.getProductDTO().getSku());
                    refundProductMessage.setSellerSkuNum(refundProductMessage.getSellerSkuNum() + 1);
                }else{
                    ReturnAndRefundProductMessage refundProductMessage = new ReturnAndRefundProductMessage();
                    refundProductMessage.setSellerSkuId(dto.getSeller_sku_id());
                    refundProductMessage.setPlatformSkuId(dto.getPlatform_sku_id());
                    refundProductMessage.setProductId(String.valueOf(dto.getProductDTO().getProduct_id()));
                    refundProductMessage.setSkuId(dto.getProductDTO().getSku());
                    refundProductMessage.setSellerSkuNum(Constant.INTEGER_ONE);
                    map.put(dto.getProductDTO().getSku(),refundProductMessage);
                }
            }
            //退款原因
            if(reasonText.size() > 0){
                returnAndRefundMessage.setReasonText(String.join("/",reasonText));
            }
            //补充sku的其他参数 商品名称 商品图片
            if(map.size() > 0){
                List<LazadaOrderItem> lazadaOrderItemList = this.getLazadaOrderItem(channelManageId, String.valueOf(lazadaReverseOrderDetailDTO.getTrade_order_id()));
                if(CollectionUtil.isNotEmpty(lazadaOrderItemList)){
                    for(LazadaOrderItem lazadaOrderItem : lazadaOrderItemList){
                        if(map.containsKey(lazadaOrderItem.getSku_id())){
                            ReturnAndRefundProductMessage refundProductMessage = map.get(lazadaOrderItem.getSku_id());
                            refundProductMessage.setProductName(lazadaOrderItem.getName());
                            refundProductMessage.setProductImageUrl(lazadaOrderItem.getProduct_main_image());
                        }
                    }
                }
                List<ReturnAndRefundProductMessage>  productList = new ArrayList<>(map.values());
                returnAndRefundMessage.setProductList(productList);
            }
        }
        log.info("convertLazadaRefundToReturnAndRefundMessage returnAndRefundMessage:{}",JSONUtil.toJsonStr(returnAndRefundMessage));
        return returnAndRefundMessage;
    }

    /**
     * {"error_msg":"null","code":"0","data":{"period_end_time":"1630339199000","max_discount_offering_money_value":"null","criteria_over_money":"100","apply":"SPECIFIC_PRODUCTS","voucher_name":"test voucher","voucher_code":"null","offering_money_value_off":"1","order_used_budget":"null","offering_percentage_discount_off":"null","period_start_time":"1626969600000","display_area":"REGULAR_CHANNEL","voucher_type":"COLLECTIBLE_VOUCHER","limit":"1","collect_start":"1626969600000","voucher_discount_type":"MONEY_VALUE_OFF","currency":"SGD","id":"91471121126083","issued":"5","status":"SUSPEND"},"success":"true","error_code":"null","request_id":"0ba2887315178178017221014"}
     * 获取lazada的优惠券信息
     * @param channelManageId
     * @param channelId
     * @param promotionId
     */
    private CouponMessage getLazadaSellerVoucher(Long channelManageId, Long channelId, String promotionId,String voucherType) {
        log.info("getLazadaSellerVoucher channelManageId:{} channelId:{} promotionId:{} voucherType:{}",channelManageId,channelId,promotionId,voucherType);
        LazadaSellerVoucherInfo LazadaSellerVoucherInfo = null;
        ApiResultVO<LazadaSellerVoucherInfo> re = null;
        try {
            re = lazadaPromotionClient.sellerVoucherDetailQuery(channelManageId, promotionId, voucherType);
            log.info("getLazadaSellerVoucher sellerVoucherDetailQuery re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                LazadaSellerVoucherInfo = re.getResult();
                return this.convertLazaddaSellerVoucherToCouponMessage(channelManageId,LazadaSellerVoucherInfo);
            }
        } catch (Exception e) {
            log.error("getLazadaSellerVoucher exception", e);
        }
        return null;
    }

    /**
     * 获取lazada的商品信息
     * @param channelManageId
     * @param channelId
     * @param param
     * @return
     */
    private ProductMessage getLazadaProduct(Long channelManageId,Long channelId,ChatCardProductParam param){
        log.info("getLazadaProduct channelManageId:{} channelId:{} param:{}",channelManageId,channelId,param);
        //设置请求参数
        GetProductItemParamDTO paramDTO = new GetProductItemParamDTO();
        paramDTO.setItem_id(Long.valueOf(param.getItemId()));
        LazadaProductItemDTO lazadaProductItemDTO = null;
        ApiResultVO<LazadaProductItemDTO> re = null;
        try {
            re = lazadaProductQueryClient.getProductItem(channelManageId, paramDTO);
            log.info("getLazadaProduct getProductItem re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                lazadaProductItemDTO = re.getResult();
                return this.convertLazadaProductToProductMessage(channelManageId,lazadaProductItemDTO,param.getActionUrl());
            }
        } catch (Exception e) {
            log.error("getLazadaProduct exception", e);
        }
        return this.convertLazadaProductToProductMessage(channelManageId, param);
    }

    /**
     * 获取lazada的商品信息  如果调用lazada三方接口 返回空或者异常时 用下边方法来组合商品消息卡片
     * @param param
     * @return
     */
    private ProductMessage convertLazadaProductToProductMessage(Long channelManageId, ChatCardProductParam param){
        ProductMessage productMessage = new ProductMessage();
        try{
            productMessage.setProductId(param.getItemId());
            productMessage.setProductImageUrl(param.getIconUrl());
            productMessage.setActionUrl(param.getActionUrl());
            productMessage.setProductName(param.getTitle());
            //获取店铺信息
            ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
            //设置币种
            String simbol = channelManageDTO.getSimbol();
            productMessage.setCurrency(simbol);
            //获取价格
            String price = this.priceHandler(param.getPrice());
            BigDecimal originPrice = StringUtils.isBlank(price) ? BigDecimal.ZERO : new BigDecimal(price);
            productMessage.setProductOriginPrice(originPrice);
            productMessage.setProductDiscountPrice(originPrice);
            productMessage.setCurrency(simbol);
            productMessage.setHasValid(true);
            log.info("convertLazadaProductToProductMessage 2 productMessage:{}",JSONUtil.toJsonStr(productMessage));
            return productMessage;
        }catch (Exception e){
            log.error("convertLazadaProductToProductMessage exception",e);
        }
        return null;
    }

    /**
     * 获取lazada的商品信息  如果调用lazada三方接口 返回空或者异常时 用下边方法来获取商品价格
     * @param priceStr
     * @return
     */
    private String priceHandler(String priceStr){
        if(ObjectUtil.isNotEmpty(priceStr)){
            if(priceStr.contains("IDR")){
                return this.getPriceStr(priceStr, "IDR");
            }else{
                for(SiteEnum siteEnum : SiteEnum.values()){
                    if(null != priceStr && priceStr.contains(siteEnum.getSimbol())){
                        return this.getPriceStr(priceStr, siteEnum.getSimbol());
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取价格
     * @param priceStr
     * @param segment
     * @return
     */
    private String getPriceStr(String priceStr, String segment){
        String[] split = priceStr.split(segment);
        String priceTmp = split[1];
        if(StringUtils.isBlank(priceTmp)){
            return null;
        }
        if(priceTmp.contains(",")){
            priceTmp = priceTmp.replace(",", "");
        }
        return priceTmp.trim();
    }
    /**
     * {"code":"0","data":{"voucher":"0.00","warehouse_code":"dropshipping","order_number":"300034416","created_at":"2014-10-15 18:36:05 +0800","voucher_code":"3432","gift_option":"0","shipping_fee_discount_platform":"0.00","customer_last_name":"last_name","updated_at":"2014-10-15 18:36:05 +0800","promised_shipping_times":"2017-03-24 16:09:22","price":"99.00","national_registration_number":"1123","shipping_fee_original":"0.00","payment_method":"COD","customer_first_name":"First Name","shipping_fee_discount_seller":"0.00","shipping_fee":"0.00","branch_number":"2222","tax_code":"1234","items_count":"1","delivery_info":"1","statuses":[],"address_billing":{"country":"Singapore","address3":"address3","address2":"address2","city":"Singapore-Central","phone":"81***8","address1":"22 leonie hill road, #13-01","post_code":"239195","phone2":"24***22","last_name":"Last Name","address5":"address5","address4":"address4","first_name":"First Name"},"extra_attributes":"{\"TaxInvoiceRequested\":\"true\"}","order_id":"16090","gift_message":"Gift","remarks":"remarks","address_shipping":{"country":"Singapore","address3":"address3","address2":"address2","city":"Singapore-Central","phone":"94236248","address1":"318 tanglin road, phoenix park, #01-59","post_code":"247979","phone2":"1******2","last_name":"Last Name","address5":"1******2","address4":"address4","first_name":"First Name"}},"request_id":"0ba2887315178178017221014"}
     * 获取lazada的订单信息
     * @param channelManageId
     * @param channelId
     * @param orderId
     */
    private OrderMeaasge getLazadaOrder(Long channelManageId,Long channelId,String orderId){
        log.info("getLazadaOrder channelManageId:{} channelId:{} orderId:{}",channelManageId,channelId,orderId);
        //设置请求参数
        LazadaOrderDetail lazadaOrderDetail = null;
        ApiResultVO<LazadaOrderDetail>  re = null;
        try {
            re = lazadaOrderOperationClient.getLazadaOrderDetail(channelManageId, orderId);
            log.info("getLazadaOrder getLazadaOrderDetail re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                lazadaOrderDetail = re.getResult();
                return this.convertLazadaOrderToOrderMeaasge(channelManageId,orderId, lazadaOrderDetail);
            }
        } catch (Exception e) {
            log.error("getLazadaOrder exception", e);
        }
        return null;
    }

    /**
     * 将lazada的商品信息转换成消息卡片所需要的信息
     * @param lazadaProductItemDTO
     */
    private ProductMessage convertLazadaProductToProductMessage(Long channelManageId, LazadaProductItemDTO lazadaProductItemDTO,String actionUrl) {
        //获取三方信息
        String productId = String.valueOf(lazadaProductItemDTO.getItem_id());
        String productName = lazadaProductItemDTO.getAttributes().get("name");
        List<String> images = lazadaProductItemDTO.getImages();
        String productImageUrl = ObjectUtil.isNotEmpty(images) ? images.get(0) : null;
        List<LinkedHashMap<String, String>> skus = lazadaProductItemDTO.getSkus();
        //转换成ChatMessageCardDTO
        ProductMessage productMessage =  new ProductMessage();
        productMessage.setProductId(productId);
        productMessage.setProductName(productName);
        productMessage.setProductImageUrl(productImageUrl);
        productMessage.setActionUrl(actionUrl);
        //从下边的路径中获取skuId
        // http://www.lazada.co.th/127881-alice-wide-pants-issa13mar-130-i4514843755-s18320318111.html
        String skuId = null;
        if(ObjectUtil.isNotEmpty(actionUrl)){
            skuId = actionUrl.substring(actionUrl.lastIndexOf("s") + 1,actionUrl.lastIndexOf("."));
        }
        if(CollectionUtil.isNotEmpty(skus)){
            for(LinkedHashMap<String, String> sku : skus){
                LazadaProductItemSkuDTO skuKey = new LazadaProductItemSkuDTO();
                String skuIdTmp = sku.get(skuKey.getSkuId());
                if(!skuId.equals(skuIdTmp)){
                    continue;
                }
                String productUrl = sku.get(skuKey.getUrl());
                String subPriceStr = sku.get(skuKey.getPrice());
                BigDecimal productOriginPrice = StringUtils.isBlank(subPriceStr) ? BigDecimal.ZERO : new BigDecimal(subPriceStr);

                String specialPriceStr = sku.get(skuKey.getSpecial_price());
                BigDecimal productDiscountPrice= StringUtils.isBlank(specialPriceStr) ? BigDecimal.ZERO : new BigDecimal(specialPriceStr);
                String subProductId = sku.get(skuKey.getSkuId());
                List<String> skuImage = JSONUtil.toList(sku.get(skuKey.getImages()), String.class);
                if(CollectionUtil.isNotEmpty(skuImage)){
                    productMessage.setProductImageUrl(skuImage.get(0));
                }
                productMessage.setSubProductId(subProductId);
                productMessage.setProductUrl(productUrl);
                productMessage.setProductOriginPrice(productOriginPrice);
                productMessage.setProductDiscountPrice(productDiscountPrice);
                //获取店铺信息
                ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
                //设置币种
                String simbol = channelManageDTO.getSimbol();
                productMessage.setCurrency(simbol);
                String areaCode = channelManageDTO.getLazadaCountry().toUpperCase();
                String special_from_time = sku.get(skuKey.getSpecial_from_time());
                String convertSpecialFromTime = null;
                String convertSpecialToTime = null;
                if(ObjectUtil.isNotEmpty(special_from_time)){
                    productMessage.setSpecialFromTime(special_from_time);
                    convertSpecialFromTime = DateUtils.dateStr2TimeOfRegion(areaCode, special_from_time);
                    productMessage.setConvertSpecialFromTime(convertSpecialFromTime);
                }
                String special_to_time = sku.get(skuKey.getSpecial_to_time());
                if(ObjectUtil.isNotEmpty(special_to_time)){
                    productMessage.setSpecialToTime(special_to_time);
                    convertSpecialToTime = DateUtils.dateStr2TimeOfRegion(areaCode, special_to_time);
                    productMessage.setConvertSpecialToTime(convertSpecialToTime);

                }
                long curTime = new Date().getTime();
                if(null != convertSpecialFromTime &&
                    null != convertSpecialToTime &&
                    curTime >= Long.valueOf(convertSpecialFromTime)
                    && curTime <= Long.valueOf(convertSpecialFromTime)){
                    //false 特价无效 true 有效
                    productMessage.setHasValid(true);
                }
            }
        }
        log.info("convertLazadaProductToProductMessage 1 productMessage:{}",JSONUtil.toJsonStr(productMessage));
        return productMessage;

    }

    /**
     * 将lazada的订单信息转换成消息卡片所需要的信息
     * @param lazadaOrderDetail
     */
    private OrderMeaasge convertLazadaOrderToOrderMeaasge(Long channelManageId,String orderId,LazadaOrderDetail lazadaOrderDetail){
        OrderMeaasge orderMeaasge = new OrderMeaasge();
        orderMeaasge.setOrderId(lazadaOrderDetail.getOrder_id());
        orderMeaasge.setOrderNumber(lazadaOrderDetail.getOrder_number());
        String apiStatus = lazadaOrderDetail.getStatuses().stream().map(String::valueOf).collect(Collectors.joining(","));
        orderMeaasge.setOrderStatus(apiStatus);
        BigDecimal orderTotalPrice = StringUtils.isBlank(lazadaOrderDetail.getPrice()) ? BigDecimal.ZERO : new BigDecimal(lazadaOrderDetail.getPrice());
        orderMeaasge.setOrderTotalPrice(orderTotalPrice);
        orderMeaasge.setItemCount(Integer.valueOf(lazadaOrderDetail.getItems_count()));
        //2014-10-15 18:36:05 +0800
        String created_at = lazadaOrderDetail.getCreated_at();
        if(ObjectUtil.isNotEmpty(created_at)){
            orderMeaasge.setOrderCreateTime(created_at);
            orderMeaasge.setConvertOrderCreateTime(DateUtils.dateStr2TimeOfTimeZone(created_at));
        }
        //获取Lazada 订单商品信息 只取第一个
        List<LazadaOrderItem> lazadaOrderItemList = this.getLazadaOrderItem(channelManageId, orderId);
        if(ObjectUtil.isNotEmpty(lazadaOrderItemList)){
            LazadaOrderItem lazadaOrderItem = lazadaOrderItemList.get(0);
            //设置商品信息
            if(ObjectUtil.isNotEmpty(lazadaOrderItem)){
                orderMeaasge.setProductName(lazadaOrderItem.getName());
                orderMeaasge.setProductMainImageUrl(lazadaOrderItem.getProduct_main_image());
            }
        }
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        orderMeaasge.setCurrency(simbol);
        log.info("convertLazadaOrderToOrderMeaasge orderMeaasge:{}",JSONUtil.toJsonStr(orderMeaasge));
        return orderMeaasge;
    }

    /**
     * 将lazada的优惠券信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param lazadaSellerVoucherInfo
     * @return
     */
    private CouponMessage convertLazaddaSellerVoucherToCouponMessage(Long channelManageId, LazadaSellerVoucherInfo lazadaSellerVoucherInfo){
        CouponMessage couponMessage = new CouponMessage();
        couponMessage.setPromotionId(String.valueOf(lazadaSellerVoucherInfo.getId()));
        //三方返回的时间 就是 时间戳型式 无需处理
        couponMessage.setStartTime(String.valueOf(lazadaSellerVoucherInfo.getPeriod_start_time()));
        couponMessage.setEndTime(String.valueOf(lazadaSellerVoucherInfo.getPeriod_end_time()));
        couponMessage.setName(lazadaSellerVoucherInfo.getVoucher_name());
        String offering_money_value_off = lazadaSellerVoucherInfo.getOffering_money_value_off();
        BigDecimal discountValue = StringUtils.isBlank(offering_money_value_off) ? BigDecimal.ZERO : new BigDecimal(offering_money_value_off);
        couponMessage.setDiscountValue(discountValue);
        couponMessage.setPercentValue(String.valueOf(lazadaSellerVoucherInfo.getOffering_percentage_discount_off()));
        couponMessage.setCouponType(lazadaSellerVoucherInfo.getVoucher_type());
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        couponMessage.setCurrency(simbol);
        couponMessage.setCurrencyCode(CountryEnum.getCountryByaAreaCode(channelManageDTO.getLazadaCountry().toUpperCase()).getCurrency_code());
        couponMessage.setStoreName(channelManageDTO.getPageName());
        couponMessage.setStoreLogoUrl(channelManageDTO.getLazadaShopLogoUrl());
        log.info("convertLazaddaSellerVoucherToCouponMessage couponMessage:{}",JSONUtil.toJsonStr(couponMessage));
        return couponMessage;
    }
    /**
     * 获取Lazada 订单商品信息 只取第一个
     * @param channelManageId
     * @param orderId
     */
    private List<LazadaOrderItem>  getLazadaOrderItem(Long channelManageId, String  orderId){
        //设置请求参数
        ApiResultVO<List<LazadaOrderItem>>  re = null;
        try {
            re = lazadaOrderOperationClient.getLazadaOrderItems(channelManageId, orderId);
            log.info("getLazadaOrderItem getLazadaOrderItems re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                return re.getResult();
            }
        } catch (Exception e) {
            log.error("getLazadaOrderItem exception", e);
        }
        return null;
    }
    //**********************************************lazada******************************************************************************//

    //**********************************************shopee******************************************************************************//

    /**
     * shopee 主商品
     * @param channelManageId
     * @param channelId
     * @param itemId
     * @return
     */
    private ProductMessage getShopeeProduct(Long channelManageId, Long channelId, String itemId) {
        log.info("getShopeeProduct channelManageId:{} channelId:{} itemId:{}",channelManageId,channelId,itemId);
        try {
            ShopeeItemBaseInfoDTO thirdShopeeProduct = getThirdShopeeProduct(channelManageId, itemId);
            if(ObjectUtil.isNotEmpty(thirdShopeeProduct)){
                return this.convertShopeeProductToProductMessage(channelManageId,thirdShopeeProduct);
            }
        } catch (Exception e) {
            log.error("getShopeeProduct exception", e);
        }
        return null;
    }

    /**
     * shopee 获取主商品
     * @param channelManageId
     * @param itemId
     * @return
     */
    private ShopeeItemBaseInfoDTO getThirdShopeeProduct(Long channelManageId, String itemId) {
        log.info("getThirdShopeeProduct channelManageId:{} itemId:{}",channelManageId,itemId);
        //设置请求参数
        ApiResultVO<List<ShopeeItemBaseInfoDTO>>  re = null;
        try {
            re = shopeeProductQueryClient.getItemBaseInfo(channelManageId, Arrays.asList(Long.valueOf(itemId)));
            log.info("getThirdShopeeProduct getItemBaseInfo re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re) && CollectionUtil.isNotEmpty(re.getResult())) {
               return  re.getResult().get(0);
            }
        } catch (Exception e) {
            log.error("getThirdShopeeProduct exception", e);
        }
        return null;
    }

    /**
     * shopee 子商品
     * @param channelManageId
     * @param itemId
     * @return
     */
    private ShopeeGetModelListResultDTO getShopeeSubProduct(Long channelManageId, Long itemId) {
        log.info("getShopeeProduct channelManageId:{} itemId:{}",channelManageId,itemId);
        //设置请求参数
        ApiResultVO<ShopeeGetModelListResultDTO>  re = null;
        try {
            re = shopeeProductQueryClient.getModelList(channelManageId, itemId);
            log.info("getShopeeSubProduct getModelList re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                return re.getResult();
            }
        } catch (Exception e) {
            log.error("getShopeeProduct exception", e);
        }
        return null;
    }

    /**
     * 将shopee的订单信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param channelId
     * @param orderId
     * @return
     */
    private OrderMeaasge getShopeeOrder(Long channelManageId, Long channelId, String orderId) {
        log.info("getShopeeOrder channelManageId:{} channelId:{} orderId:{}",channelManageId,channelId,orderId);
        //设置请求参数
        SyncFifteenDaysShopeeOrderDTO paramDto = new SyncFifteenDaysShopeeOrderDTO();
        paramDto.setChannelManageId(channelManageId);
        paramDto.setOrderCodeList(Arrays.asList(orderId));
        ShopeeOrderDetailListDTO shopeeOrderDetailListDTO = null;
        ApiResultVO<ShopeeOrderDetailListDTO>  re = null;
        try {
            re = shopeeOrderClient.getFiftyOrdersMain(paramDto);
            log.info("getLazadaOrder getLazadaOrderDetail re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                shopeeOrderDetailListDTO = re.getResult();
                return this.convertShopeeOrderToOrderMeaasge(channelManageId,shopeeOrderDetailListDTO);
            }
        } catch (Exception e) {
            log.error("getLazadaOrder exception", e);
        }
        return null;
    }

    /**
     * 将shopee的订单信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param shopeeOrderDetailListDTO
     * @return
     */
    private OrderMeaasge convertShopeeOrderToOrderMeaasge(Long channelManageId, ShopeeOrderDetailListDTO shopeeOrderDetailListDTO) {
        if(ObjectUtil.isEmpty(shopeeOrderDetailListDTO)){
            return null;
        }
        List<ShopeeOrderDetailDTO> order_list = shopeeOrderDetailListDTO.getOrder_list();
        if(CollectionUtil.isEmpty(order_list)){
            return null;
        }
        OrderMeaasge orderMeaasge = new OrderMeaasge();
        ShopeeOrderDetailDTO shopeeOrderDetailDTO = order_list.get(0);
        orderMeaasge.setOrderNumber(shopeeOrderDetailDTO.getOrder_sn());
        orderMeaasge.setOrderStatus(shopeeOrderDetailDTO.getOrder_status());
        BigDecimal totalAmount = ObjectUtil.isNotEmpty(shopeeOrderDetailDTO.getTotal_amount()) ? new BigDecimal(shopeeOrderDetailDTO.getTotal_amount()) : BigDecimal.ZERO;
        orderMeaasge.setOrderTotalPrice(totalAmount);
        orderMeaasge.setItemCount(Constant.INTEGER_ZERO);
        //shopee 创建订单时间返回时间戳
        Long created_at = shopeeOrderDetailDTO.getCreate_time();
        if(ObjectUtil.isNotEmpty(created_at)){
            orderMeaasge.setOrderCreateTime(String.valueOf(created_at));
            orderMeaasge.setConvertOrderCreateTime(String.valueOf(created_at));
        }
        //获取shopee 订单商品信息 只取第一个
        List<ShopeeOrderItemDTO> item_list = shopeeOrderDetailDTO.getItem_list();
        if(ObjectUtil.isNotEmpty(item_list)){
            orderMeaasge.setItemCount(item_list.size());
            ShopeeOrderItemDTO shopeeOrderItemDTO = item_list.get(0);
            //设置商品信息
            if(ObjectUtil.isNotEmpty(shopeeOrderItemDTO)){
                orderMeaasge.setProductName(shopeeOrderItemDTO.getItem_name());
                String productMainImageUrl = shopeeOrderItemDTO.getItemPictureUrl();
                if(ObjectUtil.isEmpty(productMainImageUrl)){
                    //通过item_id获取主图
                    productMainImageUrl = this.getProductMainImageUrl(channelManageId, String.valueOf(shopeeOrderItemDTO.getItem_id()));
                }
                orderMeaasge.setProductMainImageUrl(productMainImageUrl);
            }
        }
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        orderMeaasge.setCurrency(simbol);
        log.info("convertShopeeOrderToOrderMeaasge orderMeaasge:{}",JSONUtil.toJsonStr(orderMeaasge));
        return orderMeaasge;
    }

    /**
     * 获取主商品图片
     * @return
     */
    private String getProductMainImageUrl(Long channelManageId, String itemId){
        ShopeeItemBaseInfoDTO thirdShopeeProduct = this.getThirdShopeeProduct(channelManageId, itemId);
        if(ObjectUtil.isEmpty(thirdShopeeProduct)){
           return null;
        }
        ShopeeItemImageDTO image = thirdShopeeProduct.getImage();
        if(ObjectUtil.isEmpty(image)){
            return null;
        }
        List<String> image_url_list = image.getImage_url_list();
        if(CollectionUtil.isEmpty(image_url_list)){
            return null;
        }
        return image.getImage_url_list().get(0);
    }

    /**
     * 获取shopee的优惠券信息
     * @param channelManageId
     * @param channelId
     * @param voucherId
     * @return
     */
    private CouponMessage getShopeeSellerVoucher(Long channelManageId, Long channelId, String voucherId) {
        log.info("getShopeeSellerVoucher channelManageId:{} channelId:{} voucherId:{} ",channelManageId,channelId,voucherId);
        ShopeeVoucherDTO shopeeVoucherDTO = null;
        ApiResultVO<ShopeeVoucherDTO> re = null;
        try {
            re = shopeeVoucherClient.getVoucher(channelManageId, Long.valueOf(voucherId));
            log.info("getShopeeSellerVoucher getVoucher re:{}", JSONUtil.toJsonStr(re));
            if (this.checkResultForNoVoid(re)) {
                shopeeVoucherDTO = re.getResult();
                return this.convertShopeeSellerVoucherToCouponMessage(channelManageId,shopeeVoucherDTO);
            }
        } catch (Exception e) {
            log.error("getShopeeSellerVoucher exception", e);
        }
        return null;
    }

    /**
     * 将lazada的优惠券信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param shopeeVoucherDTO
     * @return
     */
    private CouponMessage convertShopeeSellerVoucherToCouponMessage(Long channelManageId, ShopeeVoucherDTO shopeeVoucherDTO) {
        CouponMessage couponMessage = new CouponMessage();
        couponMessage.setPromotionId(String.valueOf(shopeeVoucherDTO.getVoucher_id()));
        //三方返回的时间 就是 时间戳型式 无需处理
        couponMessage.setStartTime(String.valueOf(DateUtils.convertDiffTimeToMill(String.valueOf(shopeeVoucherDTO.getStart_time()))));
        couponMessage.setEndTime(String.valueOf(DateUtils.convertDiffTimeToMill(String.valueOf(shopeeVoucherDTO.getEnd_time()))));
        couponMessage.setName(shopeeVoucherDTO.getVoucher_name());
        //1 商铺级别的优惠劵 2 特定商品的优惠劵
        couponMessage.setShopeeVoucherType(shopeeVoucherDTO.getVoucher_type());
        if(Constant.INTEGER_TWO.equals(shopeeVoucherDTO.getVoucher_type())){
            List<Long> item_id_list = shopeeVoucherDTO.getItem_id_list();
            if(CollectionUtil.isNotEmpty(item_id_list)){
                couponMessage.setItemIds(item_id_list);
            }
        }
        //优惠折扣
        couponMessage.setRewardType(shopeeVoucherDTO.getReward_type());
        if(RewardTypeEnum.FIXAMOUNT.getCode().equals(shopeeVoucherDTO.getReward_type())){
            couponMessage.setDiscountValue(new BigDecimal(shopeeVoucherDTO.getDiscount_amount()));
            couponMessage.setPercentValue("null");
        }else if(RewardTypeEnum.DISCOUNTPERCENTAGE.getCode().equals(shopeeVoucherDTO.getReward_type())){
            couponMessage.setDiscountValue(BigDecimal.ZERO);
            couponMessage.setPercentValue(String.valueOf(shopeeVoucherDTO.getPercentage()));
        }else if(RewardTypeEnum.COIN_CASHBACK.getCode().equals(shopeeVoucherDTO.getReward_type())){
            couponMessage.setDiscountValue(BigDecimal.ZERO);
            couponMessage.setPercentValue(String.valueOf(shopeeVoucherDTO.getPercentage()));
        }
        couponMessage.setMinBasketPrice(new BigDecimal(shopeeVoucherDTO.getMin_basket_price()));
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        couponMessage.setCurrency(simbol);
        couponMessage.setCurrencyCode(CountryEnum.getCountryByaAreaCode(channelManageDTO.getShopeeRegion().toUpperCase()).getCurrency_code());
        couponMessage.setStoreName(channelManageDTO.getPageName());
        log.info("convertShopeeSellerVoucherToCouponMessage couponMessage:{}",JSONUtil.toJsonStr(couponMessage));
        return couponMessage;
    }

    private ReturnAndRefundMessage getShopeeReturnAndRefund(Long channelManageId, Long channelId, String reverseOrderId) {
        return null;
    }

    /**
     * TODO 商品卡片
     * 将shopee的商品信息转换成消息卡片所需要的信息
     * @param channelManageId
     * @param shopeeItemBaseInfoDTO
     * @return
     */
    private ProductMessage convertShopeeProductToProductMessage(Long channelManageId, ShopeeItemBaseInfoDTO shopeeItemBaseInfoDTO) {
        //获取三方信息
        String productId = String.valueOf(shopeeItemBaseInfoDTO.getItem_id());
        String productName = shopeeItemBaseInfoDTO.getItem_name();
        ShopeeItemImageDTO image = shopeeItemBaseInfoDTO.getImage();
        List<String> images = image.getImage_url_list();
        String productImageUrl = ObjectUtil.isNotEmpty(images) ? images.get(0) : null;
        //转换成ChatMessageCardDTO
        ProductMessage productMessage =  new ProductMessage();
        productMessage.setProductId(productId);
        productMessage.setProductName(productName);
        productMessage.setProductImageUrl(productImageUrl);
        List<ShopeeItemPriceInfoDTO> price_info = shopeeItemBaseInfoDTO.getPrice_info();
        if(CollectionUtil.isNotEmpty(price_info)){
            productMessage.setProductOriginPrice(price_info.get(0).getOriginal_price());
            productMessage.setProductDiscountPrice(price_info.get(0).getCurrent_price());
            productMessage.setHasValid(true);
        }
        //shopee无法从路径中获取skuId
//        String skuId = null;
//        if(ObjectUtil.isNotEmpty(actionUrl)){
//            skuId = actionUrl.substring(actionUrl.lastIndexOf("s") + 1,actionUrl.lastIndexOf("."));
//        }
        // true 有 false 无
        Boolean has_model = shopeeItemBaseInfoDTO.getHas_model();
        if(has_model){
            this.convertShopeeSubProductToProductMessage(channelManageId, shopeeItemBaseInfoDTO, productMessage);
        }
        //获取店铺信息
        ChannelManageDTO channelManageDTO = channelManageService.selectChannelManageDTOById(channelManageId);
        //设置币种
        String simbol = channelManageDTO.getSimbol();
        productMessage.setCurrency(simbol);
        log.info("convertShopeeProductToProductMessage productMessage:{}",JSONUtil.toJsonStr(productMessage));
        return productMessage;

    }

    /**
     *  子商品消息组装
     * @param channelManageId
     * @param shopeeItemBaseInfoDTO
     * @param productMessage
     */
    private void convertShopeeSubProductToProductMessage(Long channelManageId, ShopeeItemBaseInfoDTO shopeeItemBaseInfoDTO,ProductMessage productMessage){
        log.info("convertShopeeSubProductToProductMessage channelManageId:{} shopeeItemBaseInfoDTO:{} productMessage:{}",
                channelManageId, JSONUtil.toJsonStr(shopeeItemBaseInfoDTO), JSONUtil.toJsonStr(productMessage));
        ShopeeGetModelListResultDTO shopeeSubProduct = this.getShopeeSubProduct(channelManageId, shopeeItemBaseInfoDTO.getItem_id());
        if(ObjectUtil.isNotEmpty(shopeeSubProduct) && CollectionUtil.isNotEmpty(shopeeSubProduct.getModel())){
            List<ShopeeModelDTO> model = shopeeSubProduct.getModel();
            if(CollectionUtil.isNotEmpty(model)){
                ShopeeModelDTO dto = model.get(0);
                productMessage.setSubProductId(String.valueOf(dto.getModel_id()));
                String itemSubImage = this.getShopeeSubImage(dto.getTier_index(), shopeeSubProduct.getTier_variation());
                productMessage.setProductImageUrl(itemSubImage);
                BigDecimal originPrice = ObjectUtil.isNotEmpty(dto.getPrice_info()) && ObjectUtil.isNotNull(dto.getPrice_info().get(0).getOriginal_price()) ? dto.getPrice_info().get(0).getOriginal_price() : BigDecimal.ZERO;
                productMessage.setProductOriginPrice(originPrice);
                if(ObjectUtil.isNotEmpty(dto.getPrice_info()) && ObjectUtil.isNotNull(dto.getPrice_info().get(0).getCurrent_price())){
                    BigDecimal discountPrice =  dto.getPrice_info().get(0).getCurrent_price();
                    productMessage.setProductDiscountPrice(discountPrice);
                    productMessage.setHasValid(true);
                }else{
                    productMessage.setHasValid(false);
                }
            }
        }
    }
    //shopee 获取子商品规格属性的图片
    public String getShopeeSubImage(List<Integer> tier_index, List<ShopeeTierVariationDTO> tier_variation) {
        String itemSubImage = "";
        if (ObjectUtil.isEmpty(tier_index) || ObjectUtil.isEmpty(tier_variation)) {
            log.error("getShopeeSubImage tier_index " + JSONUtil.toJsonStr(tier_index) + "tier_variation " + JSONUtil.toJsonStr(tier_variation));
            return itemSubImage;
        }
        Integer firstTierOptionIndex = tier_index.get(0);
        List<ShopeeTierVariationOptionDTO> firstOptionList = tier_variation.get(0).getOption_list();
        if (firstTierOptionIndex < firstOptionList.size() && ObjectUtil.isNotNull(firstOptionList.get(firstTierOptionIndex)) && ObjectUtil.isNotNull(firstOptionList.get(firstTierOptionIndex).getImage()) && StringUtils.isNotBlank(firstOptionList.get(firstTierOptionIndex).getImage().getImage_url())) {
            itemSubImage = firstOptionList.get(firstTierOptionIndex).getImage().getImage_url();
        }
        return itemSubImage;
    }
    //****************************getChannelManageShopeeDTO******************shopee******************************************************************************//
    /**
     * channleshop返回结果判断是否成功
     *
     * @param resultVO
     * @return
     */
    private Boolean checkResultForNoVoid(ApiResultVO resultVO) {
        return ObjectUtil.isNotNull(resultVO) && resultVO.getCode() == 0 && ObjectUtil.isNotNull(resultVO.getResult());
    }
}

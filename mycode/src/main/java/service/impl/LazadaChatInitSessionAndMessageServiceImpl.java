package service.impl;

public class LazadaChatInitSessionAndMessageServiceImpl implements LazadaChatInitSessionAndMessageService {

    private static final int PAGESIZE = 20;
    //官方api 推荐 拉取30天前总数不超过3000条会话 PAGESIZE * MAXTIMES
    private static final long DAYDATEINTERVAL = 30L;
    private static final int MAXTIMES = 150;

    @Autowired
    private LazadaChatClient lazadaChatClient;

    @Autowired
    private ChatSessionService chatSessionService;

    @Resource(name = "handleMessageForLazadaExecutor")
    private ThreadPoolTaskExecutor handleMessageForLazadaExecutor;

    @Autowired
    private ChannelMessageConvertUtil channelMessageConvertUtil;

    @Autowired
    private CacheI cacheI;

    @Autowired
    private ChatInitExRecordService chatInitExRecordService;

    @Autowired
    private CommonChatService commonChatService;

    @Async("handleMessageForLazadaExecutor")
    @Override
    public void initSessionAndMessage(Long channelManageId) {
        log.info("[lazada chat] initSessionAndMessage channelManageId:{}",channelManageId);
        String lockKey = String.format(RedisKeyConstant.LAZADA_CHAT_INIT_SESSION_MESSAGE_LOCK, channelManageId);
        RedisLock redisLock = cacheI.getRedisLock(lockKey, RedisKeyConstant.LAZADA_CHAT_INIT_SESSION_MESSAGE_LOCK_ACQUIRE_TIMEOUT, RedisKeyConstant.LAZADA_CHAT_INIT_SESSION_MESSAGE_LOCK_TIMEOUT);
        if (redisLock == null){
            log.info("====================>>> [lazada chat] sessionAndMessage [initSessionAndMessage]- 【获取锁失败】，未获取到RedisLock");
            return;
        }
        long startPullTime = System.currentTimeMillis();
        try{
            log.info("====================>>> [lazada chat] sessionAndMessage [initSessionAndMessage]- 【数据处理中】");
            //构建请求参数体
            ChatInitSessionAndMessageDTO dto = commonChatService.structureChatInitSessionAndMessageDTOParam(channelManageId);
            if(ObjectUtil.isNotEmpty(dto)){
                this.handleSessionAndMessage(dto);
            }
        }catch (Exception exception){
            log.info("====================>>> [lazada chat] sessionAndMessage [initSessionAndMessage]- 【异常】，[exception=]", exception);
        }finally {
            cacheI.unRedisLock(redisLock);
            log.info("====================>>> [lazada chat] sessionAndMessage [initSessionAndMessage]- 【结束】   take time:{}", System.currentTimeMillis() - startPullTime);
        }
    }
    /**
     *
     * @param dto
     */
    @Override
    public void handleSessionAndMessage(ChatInitSessionAndMessageDTO dto) {
        log.info("[lazada chat] handleSessionAndMessage dto:{}", JSONUtil.toJsonStr(dto));
        //获取当前租户的会话信息
        try{
            //次数
            int times = 1;
            //时间
            Date date = new Date();
            Long curTime = date.getTime();
            //首次获取session
            this.handleSession(dto, date, curTime, curTime,null, PAGESIZE, times, true);
        }catch(Exception e){
            log.error("[lazada chat] handleSessionAndMessage exception", e);
        }
    }
    /**
     * 循环处理会话
     */
    private void handleSession(ChatInitSessionAndMessageDTO dto, Date date, Long curTime, Long startTime, String sessionId, Integer pageSize,int times, Boolean isFirstGetSession){
        log.info("[lazada chat] handleSession dto:{}, startTime:{}, sessionId:{}, pageSize:{}",JSONUtil.toJsonStr(dto), startTime, sessionId, pageSize);
        try{
            //调用lazada聊天getSessionList
            if(!DateUtils.isGreaterThreshold(curTime, startTime, DAYDATEINTERVAL) || times > MAXTIMES){
                log.error("[lazada chat] handleSession call getSessionList [Over a month or More than 150 times]");
                return;
            }
            try {
                Thread.sleep(RandomUtil.getRandomSleepTime());
            }catch (Exception e){
                log.error("[lazada chat] handleSession Thread sleep exception",e);
            }
            ApiResultVO<LazadaSessionDTO> sessionResult = lazadaChatClient.getSessionList(dto.getChannelManageId(), startTime, sessionId, pageSize);
            LazadaSessionDTO session = sessionResult.getResult();
            if(ObjectUtil.isNotEmpty(session)){
                List<LazadaSessionInfoDTO> session_list = session.getSession_list();
                //首次获取session 如果获取的会话为空，将此次记录下来，定时任务去完成初始化
                if(isFirstGetSession && ObjectUtil.isEmpty(session.getSession_list())){
                    log.info("[lazada chat] handleSession saveOrUpdateChatInitExRecord EMPTY 1 dto:{} ",JSONUtil.toJsonStr(dto));
                    chatInitExRecordService.saveOrUpdateChatInitExRecord(dto.getTenantId(),dto.getChannelId(), dto.getChannelManageId(), ChatInitExRecordEnum.EMPTY.getStatus());
                }
                //非首次获取的session 如果返回会话为空，结束初始化
                if(CollectionUtil.isEmpty(session_list)){
                    return;
                }
                //如果初始化状态且之前发生过异常记录信息，5分钟轮询，次数不大于5次的情况下，能够获取到session信息 将状态改成终结
                if(isFirstGetSession){
                    chatInitExRecordService.updateChatInitExRecordToFinish(dto.getTenantId(),dto.getChannelId(),dto.getChannelManageId(),ChatInitExRecordEnum.FINISH.getStatus());
                }
                for(LazadaSessionInfoDTO sessionInfo : session_list){
                    //系统会话不处理 20230725 接收 lazada 的 official 消息
//                    List<String> tags = sessionInfo.getTags();
//                    if(CollectionUtil.isNotEmpty(tags) && tags.contains("official")){
//                        log.error("[lazada chat] handleSession [this type of session is not processed] sessionId:{], tags:{}", sessionId, JSONUtil.toJsonStr(tags));
//                        continue;
//                    }
                    //异步处理session 和 mesaage
                    CompletableFuture.runAsync(() ->{
                        addSessionAndMessageToDB(dto, sessionInfo, date, curTime, startTime);
                    },handleMessageForLazadaExecutor);

                }
                times++;
                //是否还有下一页，ISV通过这个字段判断是否需要继续拉取
                if(session.getHas_more()){
                    this.handleSession(dto, date, curTime, session.getNext_start_time(), session.getLast_session_id(), pageSize,times,false);
                }
            }else{
                //争对首次获取session为空，之后通过定时任务去处理
                if(isFirstGetSession && ObjectUtil.isEmpty(session)){
                    log.info("[lazada chat] handleSession saveOrUpdateChatInitExRecord EMPTY 2 dto:{} ",JSONUtil.toJsonStr(dto));
                    chatInitExRecordService.saveOrUpdateChatInitExRecord(dto.getTenantId(),dto.getChannelId(), dto.getChannelManageId(), ChatInitExRecordEnum.EMPTY.getStatus());
                }
            }
        }catch (Exception e){
            log.error("[lazada chat] handleSession  exception", e);
            //争对首次获取session异常，之后通过定时任务去处理
            if(isFirstGetSession){
                log.info("[lazada chat] handleSession saveOrUpdateChatInitExRecord EXCEPTION dto:{} ",JSONUtil.toJsonStr(dto));
                chatInitExRecordService.saveOrUpdateChatInitExRecord(dto.getTenantId(),dto.getChannelId(), dto.getChannelManageId(), ChatInitExRecordEnum.EXCEPTION.getStatus());
            }
        }

    }

    @Transactional
    @Override
    public void addSessionAndMessageToDB(ChatInitSessionAndMessageDTO dto,LazadaSessionInfoDTO sessionInfo,Date date, Long curTime, Long startTime){
        //1、将session入库
        log.info("[lazada chat] addSessionAndMessageToDB pull message start sessionInfo:{} dto:{}, startTime:{}", JSONUtil.toJsonStr(sessionInfo), JSONUtil.toJsonStr(dto), startTime);
        long startTimeStamp = System.currentTimeMillis();
        ChatSession chatSession = chatSessionService.saveOrUpdateChatSession(dto.getTenantId(), dto.getUserId(), dto.getChannelId(), dto.getChannelManageId(), dto.getThirdPlatformId(), LazadaChatPlatformUtils.convertLazadaSessionInfoDTOToPlatformSessionDetailDTO(sessionInfo));
        //2、将message入库
        this.handleMessage(dto, sessionInfo.getSession_id(), curTime, startTime,null, PAGESIZE,sessionInfo);

        //3、更新聊天的刷新时间
        if(null != chatSession){
            chatSessionService.updateChatSession(dto.getTenantId(), dto.getUserId(), dto.getChannelId(), dto.getChannelManageId(), dto.getThirdPlatformId(),sessionInfo.getSession_id(), date);
        }
        log.info("[lazada chat] addSessionAndMessageToDB pull message end sessionId:{}, take time:{}", sessionInfo.getSession_id(), System.currentTimeMillis() - startTimeStamp);
    }

    /**
     * 分页处理message  每页数据整合完 整体落库
     */
    private void handleMessage(ChatInitSessionAndMessageDTO dto, String sessionId, Long curTime, Long startTime, String messageId, Integer pageSize,LazadaSessionInfoDTO sessionInfo){
        log.info("[lazada chat] handleMessage chatInitSessionAndMessageDTO:{}, sessionId:{}, startTime:{}, messageId:{}, pageSize:{}",JSONUtil.toJsonStr(dto), sessionId, startTime, messageId, pageSize);
        //调用lazada聊天getSessionList
        if(!DateUtils.isGreaterThreshold(curTime,startTime,DAYDATEINTERVAL)){
            log.error("[lazada chat] handleMessage  [Over a month]");
            return;
        }
        try {
            Thread.sleep(RandomUtil.getRandomSleepTime());
        }catch (Exception e){
            log.error("[lazada chat] handleSession Thread sleep exception",e);
        }
        try{
            //调用lazada聊天getMessages
            ApiResultVO<LazadaMessageDTO> messages = lazadaChatClient.getMessages(dto.getChannelManageId(), sessionId, startTime, messageId, pageSize);
            LazadaMessageDTO message = messages.getResult();
            if(ObjectUtil.isNotEmpty(message)){
                List<LazadaMessageInfoDTO> message_list = message.getMessage_list();
                if(CollectionUtil.isEmpty(message_list)){
                    return;
                }
                List<SaveOrUpdateChatMessageParam> params = new ArrayList();
                for(LazadaMessageInfoDTO messageInfo : message_list){
                    //判断每条消息是否在一个月内
                    //lazada 返回的创建时间  1687956772575
//                    if(null != messageInfo.getSend_time()){
//                        Long createdTimestamp = DateUtils.convertDiffTimeToMill(String.valueOf(messageInfo.getSend_time()));
//                        if (!DateUtils.isGreaterThreshold(curTime, createdTimestamp, DAYDATEINTERVAL)) {
//                            continue;
//                        }
//                    }

                    //构建参数 将一页数据整合完，再落库
                    SaveOrUpdateChatMessageParam param  = this.assembleMessage(sessionId, dto,messageInfo);
                    if(ObjectUtil.isNotEmpty(param)){
                        params.add(param);
                    }
                }
                if(params.size() > 0){
                    log.info("[lazada chat] receiveBatchChatMessage dto:{}, params:{}", JSONUtil.toJsonStr(dto), JSONUtil.toJsonStr(params));
                    PlatformSessionDetailDTO platformSessionDetailDTO = LazadaChatPlatformUtils.convertLazadaSessionInfoDTOToPlatformSessionDetailDTO(sessionInfo);
                    chatSessionService.receiveBatchChatMessage(dto.getTenantId(),dto.getChannelId(),dto.getChannelManageId(),dto.getThirdPlatformId(), platformSessionDetailDTO, params);
                }
                //是否还有下一页，ISV通过这个字段判断是否需要继续拉取
                if(message.getHas_more()){
                    this.handleMessage(dto, sessionId, curTime, message.getNext_start_time(), message.getLast_message_id(), pageSize,sessionInfo);
                }
                log.info("[lazada chat] pull message end sessionId:{}", sessionId);
            }
        }catch (Exception e){
            log.error("[lazada chat] handleMessage exception", e);
        }
    }

    /**
     * 组装参数 message
     * @param sessionId
     * @param messageInfo
     */
    private SaveOrUpdateChatMessageParam assembleMessage(String sessionId,ChatInitSessionAndMessageDTO dto,LazadaMessageInfoDTO messageInfo){
        SaveOrUpdateChatMessageParam param = null;
        try{
            param = channelMessageConvertUtil.convertThirdPartyMessageToChatMessage(dto.getChannelManageId(), sessionId, new JSONObject(messageInfo));
        }catch (Exception e){
            log.error("[lazada chat] assembleMessage convertThirdPartyMessageToChatMessageDTO exception", e);
        }
        return  param;
    }

}

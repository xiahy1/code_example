package service;

public interface ChatCardService {
    /**
     * 获取商品信息
     * @param channelManageId
     * @param channelId
     * @param param
     * @return
     */
    ProductMessage getProductForChat(Long channelManageId, Long channelId, ChatCardProductParam param);

    /**
     * 获取订单信息
     * @param channelManageId
     * @param channelId
     * @param orderId
     * @return
     */
    OrderMeaasge getOrderForChat(Long channelManageId, Long channelId, String orderId);

    /**
     * 获取优惠券信息
     * @param channelManageId
     * @param channelId
     * @param promotionId
     * @param voucherType 优惠券类型
     */
    CouponMessage getSellerVoucherForChat(Long channelManageId, Long channelId, String promotionId, String voucherType);

    /**
     * 获取退款信息
     * @param channelManageId
     * @param channelId
     * @param reverseOrderId
     * @return
     */
    ReturnAndRefundMessage getReturnAndRefundForChat(Long channelManageId, Long channelId, String reverseOrderId);


    /**
     * 获取视频信息
     * @param channelManageId
     * @param channelId
     * @param videoId
     * @return
     */
    VideoMessage getVideoForChat(Long channelManageId, Long channelId, String videoId);
}
